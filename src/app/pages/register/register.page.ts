import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.css'],
})
export class RegisterPage implements OnInit {

  registerError: string = ""

  // DI.
  constructor(private registerService: RegisterService, private router: Router) {}

  ngOnInit(): void {}

  onRegisterSubmit(form: NgForm): void {
    const { username, password } = form.value;
    this.registerService.register(username, password).subscribe({
      next: (response: boolean) => {
        // Assume it was successful
        console.log('REGISTER:', response);
        this.router.navigateByUrl("/movies");
      },
      error: (error) => {
        console.log(error);
        this.registerError = error;
      }
    });
  }
}