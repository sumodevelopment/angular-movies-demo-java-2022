import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from 'src/app/models/movie.model';
import { MovieDetailService } from 'src/app/services/movie-detail.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.page.html',
  styleUrls: ['./movie-detail.page.css']
})
export class MovieDetailPage implements OnInit {

  get movie(): Movie | undefined {
    return this.movieDetailService.movie
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute, 
    private movieDetailService: MovieDetailService) {
  }
  
  ngOnInit(): void {
    const movieId: string = this.route.snapshot.paramMap.get("movieId") ?? "";
    if (movieId === "") { // redundant check
      this.router.navigateByUrl("/movies")
    }
    this.movieDetailService.findMovieById(movieId);
  }

}
