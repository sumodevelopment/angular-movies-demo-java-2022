import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Movie } from 'src/app/models/movie.model';
import { MoviesService } from 'src/app/services/movies.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.page.html',
  styleUrls: ['./movies.page.css']
})
export class MoviesPage implements OnInit {

  get movies(): Movie[] {
    return this.movieService.movies;
  }

  get username(): string {
    return this.userService.user?.username || "";
  }

  // DI
  constructor(
    private userService: UserService,
    private movieService: MoviesService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.movieService.findAllMovies();
  }

  onGoToMovie(movie: Movie) {
    this.router.navigate(["/movies", movie.id]);
  }

}
