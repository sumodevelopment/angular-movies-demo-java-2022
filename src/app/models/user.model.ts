export interface User {
  id: string;
  username: string;
  firstName: string;
  lastName: string;
  address: UserAddress;
  active: number;
  createdAt: number;
  lastLogin: number;
}

export interface UserAddress {
  street1: string;
  street2: string;
  country: string;
  postalCode: string;
}
