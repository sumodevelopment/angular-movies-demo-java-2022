import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Movie } from '../models/movie.model';
import { MovieResponse } from '../models/response.model';

const URL = environment.moviesAPI;

@Injectable({
  providedIn: 'root',
})
export class MoviesService {
  private _movies: Movie[] = [];
  private _loading: boolean = false;

  // getter - Read Only
  get movies(): Movie[] {
    return this._movies;
  }

  get loading(): boolean {
    return this._loading;
  }

  // DI!
  constructor(private http: HttpClient) {}

  findAllMovies(): void {
    this._loading = true;
    this.http.get<MovieResponse<Movie[]>>(URL + "/movies")
    .pipe(
      // RxJS Operators
      map((response: MovieResponse<Movie[]>) => {
        return response.data
      }),
      finalize(() => {
        // Just before everything is over!
        this._loading = false;
      })
    )
    .subscribe({
      next: (movies: Movie[]) => {
        this._movies = movies;
      }
    });
  }
}
