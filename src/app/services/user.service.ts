import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

const USER_KEY = 'movies-user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _username: string = '';
  private _user?: User;

  get username(): string {
    return this._username;
  }

  get user(): User | undefined {
    return this._user;
  }

  set username(username: string) {
    sessionStorage.setItem(USER_KEY, username);
    this._username = username;
  }

  constructor() {
    const storedUser: string = sessionStorage.getItem(USER_KEY) ?? "";
    if (storedUser) {
      const json = JSON.parse(storedUser) as User;
      this._user = json;
    }
  }
}
