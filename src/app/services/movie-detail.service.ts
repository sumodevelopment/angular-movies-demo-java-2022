import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Movie } from '../models/movie.model';
import { MovieResponse } from '../models/response.model';

const URL = environment.moviesAPI;

@Injectable({
  providedIn: 'root'
})
export class MovieDetailService {

  private _movie?: Movie;

  get movie(): Movie | undefined {
    return this._movie;
  }

  constructor(private http: HttpClient) { }

  findMovieById(movieId: string): void {
    
    this._movie = undefined;
    
    this.http.get<MovieResponse<Movie>>(`${URL}/movies/${movieId}`)
      .pipe(
        map((response: MovieResponse<Movie>) => response.data)
      )
      .subscribe({
        next: (movie: Movie) => {
          this._movie = movie;
        }
      })
  }

}
