import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of, tap, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MovieResponse } from '../models/response.model';
import { User } from '../models/user.model';

const URL = environment.moviesAPI;

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  constructor(private http: HttpClient) {}

  private createHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': 'alskdjalksjdlkasjkdl',
    });
  }

  // Create a new user! WOW! 🤩
  register(username: string, password: string): Observable<boolean> {
    const user = {
      user: { username, password },
    };
    const headers = this.createHeaders();
    ////// RETURNING AN OBSV.
    return this.http
      .post<MovieResponse<User>>(URL + '/users/register', user, {
        headers,
      })
      .pipe(

        tap((response) => {

          if (response.success === false) {
            // Throw a new error to be handled in the catchError()
            throwError(() => new Error(response.error));
          }
          // Cause side effects in a tap. (like saving to storage).
          const user: User = response.data;
          sessionStorage.setItem("movies-user", JSON.stringify(user));

          // Note: Tap can not change the response, so no return is allowed.
        }),

        map((response: MovieResponse<User>) => {
          // Skips if there was an error thrown
          return response.success
        }),
        
        // Catch the error and use the "of" operator 
        // Throw the message we want to display in the component.
        catchError((error) => {
          throw error.error.error;
        })
      );
  }
}
