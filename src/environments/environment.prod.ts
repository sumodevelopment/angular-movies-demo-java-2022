export const environment = {
  production: true,
  moviesAPI: "https://noroff-movie-catalogue.herokuapp.com/v1"
};
